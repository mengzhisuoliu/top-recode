@echo off

echo Compiling top-recode game sources
echo ---------------------------------
echo NOTE: This script needs admin rights, otherwise symlinks
echo won't work.
echo

echo Loading Microsoft Visual Studio 2017 variables

:: Script to detect proper VS folder
if not defined PROGRAMFILES(X86) (
	set MYPROGRAMFILES=%PROGRAMFILES%
) else set MYPROGRAMFILES=%PROGRAMFILES(X86)%
echo %MYPROGRAMFILES%
if exist "%MYPROGRAMFILES%\Microsoft Visual Studio\2017\Community" (
	set MYEDITION="Community"
) else (
	if exist "%MYPROGRAMFILES%\Microsoft Visual Studio\2017\Professional" (
		set MYEDITION="Professional"
	) else (
		if exist "%MYPROGRAMFILES%\Microsoft Visual Studio\2017\Enterprise" set MYEDITION="Enterprise"
	)
)

call "%MYPROGRAMFILES%\Microsoft Visual Studio\2017\%MYEDITION%\VC\Auxiliary\Build\vcvarsall.bat" x86

:: (fix for Windows 8+) Change directory to the one script is on
cd /d %~dp0 

echo Cloning luajit
cd source\src
call git clone http://luajit.org/git/luajit-2.0.git luajit
cd ..\..
copy compatlj52-msvcbuild.bat source\src\luajit\src
cd source\src\luajit
call git checkout v2.1

echo Compiling luajit
cd src
call compatlj52-msvcbuild.bat
cd ..\..\..

echo Make symbolic links for luajit binaries for project building
cd lib
mkdir luajit
cd luajit
mklink "lua51.dll" "..\..\src\luajit\src\lua51.dll"
mklink "lua51.lib" "..\..\src\luajit\src\lua51.lib"
mklink "luajit.lib" "..\..\src\luajit\src\luajit.lib"
cd ..\..

cd inc
mkdir luajit
cd luajit
mklink "lauxlib.h" "..\..\src\luajit\src\lauxlib.h"
mklink "lua.h" "..\..\src\luajit\src\lua.h"
mklink "lua.hpp" "..\..\src\luajit\src\lua.hpp"
mklink "luaconf.h" "..\..\src\luajit\src\luaconf.h"
mklink "luajit.h" "..\..\src\luajit\src\luajit.h"
mklink "lualib.h" "..\..\src\luajit\src\lualib.h"
cd ..\..\..

echo Compiling all projects belonging to project (this will take a long time!)
devenv /build Release ".\source\src\all.sln"
devenv /build "Release Client" ".\source\src\all.sln"

echo Make symbolic links for all the binaries created
cd client\system
mklink "lua51.dll" "..\..\source\lib\luajit\lua51.dll"
mklink "Game.exe" "..\..\source\bin\Game.exe"
mklink "Game.pdb" "..\..\source\bin\Game.pdb"
mklink "MindPower3D_D8R.dll" "..\..\source\lib\LightEngine\MindPower3D_D8R.dll"
mklink "MindPower3D_D8R.pdb" "..\..\source\lib\LightEngine\MindPower3D_D8R.pdb"
cd ..\..

cd server
mklink "AccountServer.exe" "..\source\bin\AccountServer.exe"
mklink "GameServer.exe" "..\source\bin\GameServer.exe"
mklink "GateServer.exe" "..\source\bin\GateServer.exe"
mklink "GroupServer.exe" "..\source\bin\GroupServer.exe"
mklink "lua51.dll" "..\source\lib\luajit\lua51.dll"
cd ..

cd helpers
mklink "luajit.exe" "..\..\src\luajit-2.0\src\luajit.exe"
mklink "lua51.dll" "..\..\src\luajit-2.0\src\lua51.dll"
cd ..

echo Make symbolic links for icu59
cd translation
mklink "icudt59.dll" "..\source\lib\icu\icudt59.dll"
mklink "icuin59.dll" "..\source\lib\icu\icuin59.dll"
mklink "icuio59.dll" "..\source\lib\icu\icuio59.dll"
mklink "icutu59.dll" "..\source\lib\icu\icutu59.dll"
mklink "icuuc59.dll" "..\source\lib\icu\icuuc59.dll"
cd ..

echo Running prepare.bat
call prepare.bat

@echo on