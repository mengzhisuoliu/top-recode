--TODO: Import mission list from server.
--TODO: Get newer client files so this file won't be needed.
--TODO: Make skins completely independent, and make it use defaults when certain items are not implemented.
--TODO: Rewrite GUI scripts.

--[[
This function is an extension to the original tostring function, and returns
the string representation of many values including tables.
--]]

--function mytostring(x)
--	local s
--	if type(x) == "table" then
--		s = "{"
--		j = 0
--		for i, v in pairs(x) do
--			if i ~= "_G" and v ~= x then
--				if type(i) ~= "number" then
--					temp = "['" .. tostring(i) .. "']="
--				else
--					temp = "[" .. tostring(i) .. "]="
--				end
--				if ((type(rawget (x, i)) == "table") or (type(rawget (x, i)) == "number")) then
--					temp = temp .. mytostring(rawget (x, i))
--				else
--					temp = temp .. "'" .. mytostring(rawget (x, i)).. "'"
--				end
--				
--				if j ~= 0 then
--					s = s .. "," .. temp
--				else
--					s = s .. temp
--				end
--				j = j + 1
--			end
--		end
--		return s .. "}"
--	else return tostring(x)
--	end
--end
--
--function myprint(x)
--	print(mytostring(x))
--end

GetResString = UI_ResString --dunno if needed here

-- Mission

print( "loading mission.lua" )

--TODO: Make server mission list go here somewhere

C_FALSE					= 0
C_TRUE					= 1

MIS_TREENODE_INVALID	= 0
MIS_TREENODE_NOMAL		= 1
MIS_TREENODE_HISTORY	= 2
MIS_TREENODE_GUILD		= 3
MIS_TREENODE_MAIN		= 4

MisData = {}

function AddMisData( id, name, tp, x1, y1, map1, x2, y2, map2 )
	if id == nil or name == nil or tp == nil then
		MISSION_MISSION_LUA_000001 = GetResString("MISSION_MISSION_LUA_000001")
		print( MISSION_MISSION_LUA_000001, id, name, tp )
		return
	end

	if MisData[id] ~= nil then
		MISSION_MISSION_LUA_000002 = GetResString("MISSION_MISSION_LUA_000002")
		print( MISSION_MISSION_LUA_000002, id, MisData[id].name, MisData[id].tp, id, name, tp )
	end

	MisData[id] = {}
	MisData[id].name = name
	MisData[id].tp = tp
	MisData[id].x1 = x1
	MisData[id].y1 = y1
	MisData[id].map1 = map1
	MisData[id].x2 = x2
	MisData[id].y2 = y2
	MisData[id].map2 = map2
end

AddMisDataFromLua = AddMisData

function GetMisData( id )
	if id == nil then
		MISSION_MISSION_LUA_000003 = GetResString("MISSION_MISSION_LUA_000003")
		print( MISSION_MISSION_LUA_000003 )
		return C_FALSE
	end

	if MisData[id] == nil or MisData[id].name == nil or MisData[id].tp == nil then
		MISSION_MISSION_LUA_000004 = GetResString("MISSION_MISSION_LUA_000004")
		print( MISSION_MISSION_LUA_000004..id )
		return C_FALSE
	end
	
	return C_TRUE, MisData[id].tp, MisData[id].name, MisData[id].x1, MisData[id].y1, MisData[id].map1, MisData[id].x2, MisData[id].y2, MisData[id].map2
end

MISSION_MISSION_LUA_000005 = GetResString("MISSION_MISSION_LUA_000005")
MISSION_MISSION_LUA_000005 = GetResString("MISSION_MISSION_LUA_000005")
MISSION_MISSION_LUA_000006 = GetResString("MISSION_MISSION_LUA_000006")
AddMisDataFromLua( 100, MISSION_MISSION_LUA_000006, MIS_TREENODE_NOMAL,0,0,MISSION_MISSION_LUA_000005,0,0,MISSION_MISSION_LUA_000005 )

function _te(table,else_part)
	return setmetatable(table,
		{__index = function(table,key)
			local v = rawget(table, key)
			if v == nil then
				if else_part == nil then
					v = key
				else
					v = else_part
				end
			end
			return v
		end})
end

 --Disabled in game client source code
function GetPreviouName(lEnergy, hEnergy, sex)
	return _te({
		"255,0,0",
		"255,255,0",
		"0,255,0",
		"0,0,255",
		"0,0,0",
		"255,255,255",
		"255,0,255",
		"128,128,128",
		"255,128,128",
		"255,128,0",
		"0,128,128",
		"0,128,0",
		"255,0,128",
		"0,255,255",
		"0,128,255"
	},"0,0,0")[lEnergy-6000], GetResString(_te({
		"SCENE_PREVIOUNAME_CLU_000002",
		"SCENE_PREVIOUNAME_CLU_000003",
		"SCENE_PREVIOUNAME_CLU_000004",
		"SCENE_PREVIOUNAME_CLU_000005",
		"SCENE_PREVIOUNAME_CLU_000006",
		"SCENE_PREVIOUNAME_CLU_000007",
		"SCENE_PREVIOUNAME_CLU_000008",
		"SCENE_PREVIOUNAME_CLU_000009",
		"SCENE_PREVIOUNAME_CLU_000010",
		"SCENE_PREVIOUNAME_CLU_000011",
		"SCENE_PREVIOUNAME_CLU_000012",
		"SCENE_PREVIOUNAME_CLU_000013",
		"SCENE_PREVIOUNAME_CLU_000014",
		"SCENE_PREVIOUNAME_CLU_000015",
		"SCENE_PREVIOUNAME_CLU_000016",
		"SCENE_PREVIOUNAME_CLU_000017",
		"SCENE_PREVIOUNAME_CLU_000018",
		"SCENE_PREVIOUNAME_CLU_000019",
		"SCENE_PREVIOUNAME_CLU_000020",
		"SCENE_PREVIOUNAME_CLU_000021",
		"SCENE_PREVIOUNAME_CLU_000022",
		"SCENE_PREVIOUNAME_CLU_000023",
		"SCENE_PREVIOUNAME_CLU_000024",
		"SCENE_PREVIOUNAME_CLU_000025",
		"SCENE_PREVIOUNAME_CLU_000026",
		"SCENE_PREVIOUNAME_CLU_000027",
		"SCENE_PREVIOUNAME_CLU_000028",
		"SCENE_PREVIOUNAME_CLU_000029",
		"SCENE_PREVIOUNAME_CLU_000030",
		"SCENE_PREVIOUNAME_CLU_000031",
		"SCENE_PREVIOUNAME_CLU_000032",
		"SCENE_PREVIOUNAME_CLU_000033",
		"SCENE_PREVIOUNAME_CLU_000034",
		"SCENE_PREVIOUNAME_CLU_000035",
		"SCENE_PREVIOUNAME_CLU_000036",
		"SCENE_PREVIOUNAME_CLU_000037",
		"SCENE_PREVIOUNAME_CLU_000038",
		"SCENE_PREVIOUNAME_CLU_000039",
		"SCENE_PREVIOUNAME_CLU_000040",
		"SCENE_PREVIOUNAME_CLU_000041",
		"SCENE_PREVIOUNAME_CLU_000042",
		"SCENE_PREVIOUNAME_CLU_000043",
		"SCENE_PREVIOUNAME_CLU_000044",
		"SCENE_PREVIOUNAME_CLU_000045",
		"SCENE_PREVIOUNAME_CLU_000046",
		"SCENE_PREVIOUNAME_CLU_000047",
		"SCENE_PREVIOUNAME_CLU_000048",
		"SCENE_PREVIOUNAME_CLU_000049",
		"SCENE_PREVIOUNAME_CLU_000050",
		"SCENE_PREVIOUNAME_CLU_000051",
		"SCENE_PREVIOUNAME_CLU_000052",
		"SCENE_PREVIOUNAME_CLU_000053",
		"SCENE_PREVIOUNAME_CLU_000054",
		"SCENE_PREVIOUNAME_CLU_000055",
		"SCENE_PREVIOUNAME_CLU_000056",
		"SCENE_PREVIOUNAME_CLU_000057",
		"SCENE_PREVIOUNAME_CLU_000058",
		"SCENE_PREVIOUNAME_CLU_000059",
		"SCENE_PREVIOUNAME_CLU_000060",
		"SCENE_PREVIOUNAME_CLU_000061",
		"SCENE_PREVIOUNAME_CLU_000062",
		"SCENE_PREVIOUNAME_CLU_000063",
		"SCENE_PREVIOUNAME_CLU_000064",
		"SCENE_PREVIOUNAME_CLU_000065",
		"SCENE_PREVIOUNAME_CLU_000066",
		"SCENE_PREVIOUNAME_CLU_000067",
		"SCENE_PREVIOUNAME_CLU_000068",
		"SCENE_PREVIOUNAME_CLU_000069",
		"SCENE_PREVIOUNAME_CLU_000070",
		"SCENE_PREVIOUNAME_CLU_000071",
		"SCENE_PREVIOUNAME_CLU_000072",
		"SCENE_PREVIOUNAME_CLU_000073",
		"SCENE_PREVIOUNAME_CLU_000074",
		"SCENE_PREVIOUNAME_CLU_000075",
		"SCENE_PREVIOUNAME_CLU_000076",
		"SCENE_PREVIOUNAME_CLU_000077",
		"SCENE_PREVIOUNAME_CLU_000078",
		"SCENE_PREVIOUNAME_CLU_000079",
		"SCENE_PREVIOUNAME_CLU_000080",
		"SCENE_PREVIOUNAME_CLU_000081",
		"SCENE_PREVIOUNAME_CLU_000082",
		"SCENE_PREVIOUNAME_CLU_000083",
		"SCENE_PREVIOUNAME_CLU_000084",
		"SCENE_PREVIOUNAME_CLU_000085",
		"SCENE_PREVIOUNAME_CLU_000086",
		"SCENE_PREVIOUNAME_CLU_000087",
		"SCENE_PREVIOUNAME_CLU_000088",
		"SCENE_PREVIOUNAME_CLU_000089",
		"SCENE_PREVIOUNAME_CLU_000090",
		"SCENE_PREVIOUNAME_CLU_000091",
		"SCENE_PREVIOUNAME_CLU_000092",
		"SCENE_PREVIOUNAME_CLU_000093",
		"SCENE_PREVIOUNAME_CLU_000094",
		"SCENE_PREVIOUNAME_CLU_000095",
		"SCENE_PREVIOUNAME_CLU_000096",
		"SCENE_PREVIOUNAME_CLU_000097",
		"SCENE_PREVIOUNAME_CLU_000098",
		"SCENE_PREVIOUNAME_CLU_000099",
		"SCENE_PREVIOUNAME_CLU_000100",
		"SCENE_PREVIOUNAME_CLU_000101",
		"SCENE_PREVIOUNAME_CLU_000102"
	},"SCENE_PREVIOUNAME_CLU_000001")[hEnergy-6000+1]) --+1 to make perfect enumeration (won't need [])
end