--[[
This function is an extension to the original tostring function, and returns
the string representation of many values including tables.
--]]

--print('test')
--
--function mytostring(x)
--	local s
--	if type(x) == "table" then
--		s = "{"
--		j = 0
--		for i, v in pairs(x) do
--			if i ~= "_G" and v ~= x then
--				if type(i) ~= "number" then
--					temp = "['" .. tostring(i) .. "']="
--				else
--					temp = "[" .. tostring(i) .. "]="
--				end
--				if ((type(rawget (x, i)) == "table") or (type(rawget (x, i)) == "number")) then
--					temp = temp .. mytostring(rawget (x, i))
--				else
--					temp = temp .. "'" .. mytostring(rawget (x, i)).. "'"
--				end
--				
--				if j ~= 0 then
--					s = s .. "," .. temp
--				else
--					s = s .. temp
--				end
--				j = j + 1
--			end
--		end
--		return s .. "}"
--	else return tostring(x)
--	end
--end

--function myprint(x)
--	print(mytostring(x))
--end

--myprint(_G)

TRUE = 1
FALSE = 0

GetResString = UI_ResString

-- scene

-------------------------------------------------------------
-- 场景类
-------------------------------------------------------------
enumEQUIP_HEAD		= 0 	-- 五个身体部位,头,脸,身体,手,脚
enumEQUIP_FACE		= 1
enumEQUIP_BODY		= 2
enumEQUIP_GLOVE		= 3	-- 手套
enumEQUIP_SHOES		= 4	-- 鞋子

enumEQUIP_LHAND		= 6	-- 左手		-- 左手右手的道具值为客户端Link点
enumEQUIP_RHAND		= 9	-- 右手

enumEQUIP_NECK		= 5	-- 脖子:带项链,徽章
enumEQUIP_HAND1		= 7	-- 两个手饰
enumEQUIP_HAND2		= 8

enumEQUIP_Jewelry1	= 10
enumEQUIP_Jewelry2	= 11
enumEQUIP_Jewelry3	= 12
enumEQUIP_Jewelry4      = 13

-- 创建一个场景

PLAY_ONCE = 1
PLAY_LOOP = 2
PLAY_FRAME = 3
PLAY_ONCE_SMOOTH = 4
PLAY_LOOP_SMOOTH = 5
PLAY_PAUSE = 6
PLAY_CONTINUE = 7
PLAY_INVALID = 0

-------------------------------------------------------------
-- ??
-------------------------------------------------------------
-- ?????????,??????????????

enumLoginScene    =0
enumWorldScene    =1		-- ????
enumSelectChaScene = 2		-- ????
enumCreateChaScene = 3		-- ????

SN_SetAttackChaColor(255, 180, 180)

-- face

local i
for i=0,24 do
	UI_LoadHeadSayFaceImage( i, 2, 50, 50, "texture/ui/brow/"..i..".tga", 64, 64, 0, 0 )
end

--added these 2
UI_LoadHeadSayFaceImage( 25, 2, 50, 50, "texture/ui/brow/brow11.tga", 64, 64, 0, 0)
UI_LoadHeadSayFaceImage( 26, 2, 50, 50, "texture/ui/brow/brow12.tga", 64, 64, 0, 0)


-- ??
UI_LoadHeadSayLifeImage( 60, 5, "texture/ui/system/life.tga", 60, 5, 0, 0, TRUE )
--  ???

--  ?????????
UI_LoadHeadSayShopImage( 0,  15, 34, "texture/ui/BoothName.tga", 15, 34, 0, 0 )
UI_LoadHeadSayShopImage( 1,  1, 34, "texture/ui/BoothName.tga", 1, 34, 18, 0 )
UI_LoadHeadSayShopImage( 2,  15, 34, "texture/ui/BoothName.tga", 15, 34, 22, 0 )

CHA_SetClientAttr(1, 30, 1.5, 0.5)
CHA_SetClientAttr(2, 30, 1.8, 0.4)
CHA_SetClientAttr(3, 30, 1.5, 0.3)
CHA_SetClientAttr(4, 30, 1.4, 0.5)