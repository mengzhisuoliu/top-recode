apple = {}
apple_cnt = 0
apple_size = 6

apple_funcs = {
	[true] = {{lua_fox_boff,true}, {lua_dog_blog,false}},
	[false] = {{lua_dog_brog,false}, {lua_fox_boff,true}}
}

function _fv(b, b2, p, w, v)
	apple_funcs[b][b2][1](p, w, v)
end

function _fv2(b, b2, p, w, _v1, _v2)
	local t = {[true]=_v1,[false]=_v2}
	_fv(b, b2, p, w, t[apple_funcs[b][b2][2]])
end

_k = {0,0,0,0,0,0}
_v = {0,0,0,0,0,0}

apple_loop1 = function(loop, b, p)
	for j=0,loop-1 do
		local w = j * apple_size
		for i=1,apple_size do
			_fv2(b, 1, p, w, _k[i], _v[i])
			_fv2(b, 2, p, w, _k[i], _v[i])
			w = w + 1
		end
	end
end

apple_loop2 = function(loop, rcnt, b, p)
	for i=1,rcnt do
		local w = loop * apple_cnt + i - 1
		local a, v = lua_cat_fish(apple[i], apple_cnt)
		v = v + 1
		_fv2(b, 1, p, w, apple[i], v)
		_fv2(b, 2, p, w, apple[i], v)
	end
end

apple_eat = function(p, l, b)
	local loop, rcnt = lua_cat_fish(l, apple_cnt)
	for j=0,loop-1 do
		apple_loop2(j, apple_cnt, b, p)
	end
	apple_loop2(loop, rcnt, b, p)
end

apple_eat2 = function(p, l, b)
	local loop, rcnt = lua_cat_fish(l, apple_cnt)
	apple_loop1(loop, b, p)
	apple_loop2(loop, rcnt, b, p)
end

apple_eat2_1 = function(p, l, b)
	local loop, rcnt = lua_cat_fish(l, apple_size)
	apple_loop1(loop, b, p)
	local w = loop * apple_size
	for i=1,apple_size-1 do --yes, it only uses the first 5 items here
		_fv(b, 1, p, w, _k[i])
		_fv(b, 2, p, w, _v[i])
		w = w + 1
		if i > rcnt then
			break
		end
	end
end

apple_allot = function(c, i)
	local a_
	i = i + 1
	apple[i] = c
	apple_cnt = i
	if i <= apple_size then
		_k[i] = c
		a_, _v[i] = lua_cat_fish(c, apple_size)
		_v[i] = _v[i] + 1
	else
		print("wrong index")
	end
end