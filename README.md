# top-recode

Objective is to recode TOP/PKO .lua files in a way the syntax becomes easier to use, or that implements features that can provide more freedom of coding.

This is only intended for testing. Everything here is meant to be used in your own computer (no website for example): including the server and client.

PKO 2.7 mod is used. My default mod is still on alpha and is not supposed to be used yet.

## Smaller goals:
- Priority: Many crashes client is experiencing are based on TCHAR based functions interacting with regular chars on a multi-byte encoding compilation. An example is
with textures not appearing bug. I changed all chars to TCHAR and it worked perfectly. But TCHAR is not best solution. This will have to be changed.
- Implement DirectX 9 - it is already partially implemented (from the base code).

## WINDOWS:

### How to install:
1. Install Microsoft Visual Studio 2017 Community Edition. Then on Visual Studio Installer get: VC C++ 2017 v141 toolset, C++ Profiling tools, Windows 10 SDK (latest) for Desktop C++, Visual C++ tools for CMake (not needed yet I think), Visual C++ ATL support.
2. Install and configure MSQL of any version liked - go to pkodev guide section for that. Don't do anything with accounts, dbs, ip or more yet
3. install git for windows. Select the cmd.exe options (but no options with added unix tools please). Then clone:
4. (using cmd.exe) cd to the folder where you want the project to be, and type:
git clone "https://gitlab.com/deguix/top-recode"
5. run with Admin priviledges the "compile_src.bat" in the main folder (symlink creation requires admin priviledges). This will take a long time in slow computers.
6. (if using MSQL 2000) copy files in db_2000 to dbs_in_use folder. In all .cfg files in server folder, remove all ",1433" mentions.
7. Give user full control of the dbs_in_use folder.
8. Make sure MSQL is started. Following the account creation part in the MSQL guide, create pko_game and pko_account users (for gameserver and accountserver respectivelly) with the regular password.
9. Following the db attachment in MSQL guide, attach the dbs that are in the dbs_in_use folder - on the attach window, after "opening" the files, assign gameserver db ownership to pko_game and accountserver's to pko_account.
10. Don't follow any ip editting steps or website steps - files are already configured for local use.

### How to update without any other actions (if for some reason compile_src.bat fails and online git files are newer):
1. (using cmd.exe) cd to the project folder, and type:
git pull

### How to update and compile src files (assumes compile_src.bat completes successfully once):
2. run update.bat in main folder (no need for admin priviledges, no symlink creation here). Compilation might take a long time.

### How to use (assumes compile_src.bat completes successfully once):
1. Start the SQL server (if it isn't by now) and start runall.bat to start all game server exe's.
2. Run run.bat to run client.
3. Select the only server available, and put the user name *deguix* and pass *123456* and press Enter. The game should be working at this point.

## LINUX:

Copy or symlink folder from Windows. Unfortunatelly the source can only be compiled on Windows.


## Additional: How to adapt mod files to use this source:
### Server lua files:
1. Make sure the scripts work on TOP2 server.
2. Copy scripts folder from the resource folder of your mod, and paste in inside this projects resource/scripts folder.
3. Rename it to whatever mod name you would like.
4. Copy initial.lua from pko_2.7 (it's a compability mod) to your recently copied mod folder.
5. Create a "maps" folder in your mod folder.
6. Create a folder for each map with its name in the "maps" folder.
7. Copy each map folder .lua files in your original files to respective map folders (that are in your "map folder).
8. (TO BE CHANGED - TODO: Give each mod independence) Copy the .txt and other files (not .lua) of your map folders to the respective map folders in the resource folder.
9. Replace all "local count = ReadByte( rpk )" with local count = ReadWord( rpk )" in ncpsdk.lua (higher item buy/sale limits).

### Client files (TO BE CHANGED - TODO: Give each mod independence):
1. Lua files have no compatibility yet (and no missions/gems yet).
2. Replace table folder of these files with your own mod's.
3. Replace any other files except lua files and system folder files with your own.

### Publishing:
1. When zipping the files, the symlinks will resolve.