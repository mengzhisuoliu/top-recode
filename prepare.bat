@echo off

echo Preparing top-recode
echo --------------------
echo NOTE: This script needs admin rights, otherwise symlinks
echo won't work.
rem echo
rem echo NOTE: This script will only be able to create and compile tsv
rem echo txt files if SQL Server is running.
rem echo

:: (fix for Windows 8+) Change directory to the one script is on
cd /d %~dp0 

echo Compiling translation resources...
cd translation
call compile.bat
cd ..

echo Symbolic/hard linking files (only works on Vista and above)...
cd server
mklink "en_US.res" "..\translation\en_US.res"
mklink "pt_BR.res" "..\translation\pt_BR.res"
mklink "icudt59.dll" "..\translation\icudt59.dll"
mklink "icuin59.dll" "..\translation\icuin59.dll"
mklink "icuuc59.dll" "..\translation\icuuc59.dll"
mklink "AccountServer.loc" "..\translation\Game.loc"
mklink "GameServer.loc" "..\translation\Game.loc"
mklink "GateServer.loc" "..\translation\Game.loc"
mklink "GroupServer.loc" "..\translation\Game.loc"
cd ..\client\system
mklink "en_US.res" "..\..\translation\en_US.res"
mklink "pt_BR.res" "..\..\translation\pt_BR.res"
mklink "icudt59.dll" "..\..\translation\icudt59.dll"
mklink "icuin59.dll" "..\..\translation\icuin59.dll"
mklink "icutu59.dll" "..\..\translation\icutu59.dll"
mklink "icuuc59.dll" "..\..\translation\icuuc59.dll"
mklink "Game.loc" "..\..\translation\Game.loc"
cd ..\scripts
mklink /J "table" "table-top2"
cd ..\..\resource
mklink "areaset.txt" "..\client\scripts\table\areaset.txt"
mklink "characterinfo.txt" "..\client\scripts\table\characterinfo.txt"
mklink "forgeitem.txt" "..\client\scripts\table\forgeitem.txt"
mklink "iteminfo.txt" "..\client\scripts\table\iteminfo.txt"
mklink "shipinfo.txt" "..\client\scripts\table\shipinfo.txt"
mklink "shipiteminfo.txt" "..\client\scripts\table\shipiteminfo.txt"
mklink "skillinfo.txt" "..\client\scripts\table\skillinfo.txt"
mklink "skilleff.txt" "..\client\scripts\table\skilleff.txt"
cd ..

rem This part is only used if needing default mod
rem
rem echo Touching files so that new ones can be created by running gameserver...
rem cd client\scripts\table
rem echo $null >> "areaset.txt"
rem echo $null >> "characterinfo.txt"
rem echo $null >> "iteminfo.txt"
rem echo $null >> "shipinfo.txt"
rem echo $null >> "shipiteminfo.txt"
rem echo $null >> "skilleff.txt"
rem echo $null >> "skillinfo.txt"
rem cd ..\lua\table
rem echo $null >> "stonehint_raw.lua"
rem cd ..\..\..\..\resource/script/default/raw/
rem echo $null >> "npcs.lua"
rem echo $null >> "skills.lua"
rem echo $null >> "skill_books.lua"
rem cd ..\..\..\..
rem 
rem echo Running game server if SQL Server is running
rem echo Please close gameserver if it keeps running
rem cd server
rem GameServer.exe
rem cd ..

echo Copy dbs to db_in_use folder (to prevent conficts with any db updates)...
mkdir db_in_use
copy /y db_2008\accountserver.mdf db_in_use\accountserver.mdf
copy /y db_2008\accountserver_1.ldf db_in_use\accountserver_1.ldf
copy /y db_2008\gamedb.mdf db_in_use\gamedb.mdf
copy /y db_2008\gamedb_1.ldf db_in_use\gamedb_1.ldf

echo Compiling game table files... Press OK on dialog boxes.
cd client
call compile.bat
cd ..
echo Finished.
pause
@echo on
