--Adaptations to luajit:
---FIXED: Replaced all math.mod to math.fmod.
---FIXED: Replaced all table.getn(table) to #table.
---FIXED: Replaced all table.n to #table (n wasn't set either, so its equivalent to above).
---FIXED: Added "local arg = table.pack(...)" for the functions that use vararg syntax from lua 5.0.

--Adaptations to server files - which are needed to be compatible with the server files:
---CHANGED: initial.lua is needed.
---CHANGED: init.lua is needed for map load (instead of entry.lua and ctrl.lua), and init_copy.lua is needed for map_copy load (instead of *entity.lua and *monster_conf.lua)
---CHANGED: change all in npcsdk.lua:
--- local count = ReadByte( rpk )
---to:
--- local count = ReadWord( rpk )

print(' ')
print('Loading "'..mod..'" mod')
print('------------------------------------')

print('Loading pre-map handling files')
-- Load all other files first
local filelist = {
	'MisSdk/npcsdk.lua',
	'MisSdk/missionsdk.lua',
	'MisSdk/scriptsdk.lua',
	--kop scripts
	'help/help.lua',
	
	'birth/birth_conf.lua',

	'ai/ai_define.lua',
	'ai/ai_sdk.lua',
	'ai/ai.lua',

	'calculate/exp_and_level.lua',
	'calculate/JobType.lua',
	'calculate/AttrType.lua',
	'calculate/Init_Attr.lua',
	'calculate/ItemAttrType.lua',
	'calculate/functions.lua',
	'calculate/AttrCalculate.lua',
	'calculate/ItemEffect.lua',
	'calculate/variable.lua',
	'calculate/Look.lua',
	'calculate/forge.lua',
	'calculate/ItemGetMission.lua',
	'calculate/skilleffect.lua',

	'MisScript/templatesdk.lua',
	'MisScript/NpcDefine.lua',
	'MisScript/ScriptDefine.lua',
	'MisScript/NpcScript08.lua',
	'MisScript/NpcScript07.lua',
	'MisScript/NpcScript06.lua',
	'MisScript/NpcScript05.lua',
	'MisScript/NpcScript04.lua',
	'MisScript/NpcScript03.lua',
	'MisScript/NpcScript02.lua',
	'MisScript/NpcScript01.lua',
	'MisScript/EudemonScript.lua',
	'MisScript/SendMission.lua',
	'MisScript/MissionScript08.lua',
	'MisScript/MissionScript07.lua',
	'MisScript/MissionScript06.lua',
	'MisScript/MissionScript05.lua',
	'MisScript/MissionScript04.lua',
	'MisScript/MissionScript03.lua',
	'MisScript/MissionScript02.lua',
	'MisScript/MissionScript01.lua',
	'monster/mlist.lua'
}

function r()
	--	start_time = os.clock()
	--	print('Loading main scripts')
	for i,filepath in ipairs(filelist) do
		print('- Loading file: '..filelist[i])
		dofile(GetResPath('script/'..mod..'/'..filelist[i]))
	end
	--	Notice('Recompiled scripts in '..os.difftime(os.clock(), start_time).. 's.') -- Notice cannot be run in first execution (causes crash).
end
r()