@echo off

echo Updating top-recode
echo -------------------

:: (fix for Windows 8+) Change directory to the one script is on
cd /d %~dp0

echo Loading Microsoft Visual Studio 17 variables
call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x86

echo Pulling from git...
call git pull

echo Pulling luajit from git...
cd source\src\luajit
call git pull

echo Compiling luajit
cd src
call compatlj52-msvcbuild.bat
cd ..\..\..\..

echo Compiling all projects belonging to project (this will take a long time!)
devenv /build Release ".\source\src\all.sln"
devenv /build "Release Client" ".\source\src\all.sln"

echo Compiling translation resources...
cd translation
call compile.bat
cd ..

rem This part is only used if needing default mod
rem 
rem echo Running game server if SQL Server is running
rem echo Please close gameserver if it keeps running
rem cd server
rem GameServer.exe
rem cd ..

echo Compiling game table files... Press OK on dialog boxes.
cd client
call compile.bat
cd ..
echo Finished.
pause
@echo on