#ifndef _KOP_LICENSE_H_
#define _KOP_LICENSE_H_

#include <windows.h>
#include <wininet.h>
#include <openssl/rsa.h>
#include <openssl/bio.h>
#include <openssl/pem.h>

#include <string>

//#pragma comment(lib,"libeay32")//openssl
//#pragma comment(lib, "wininet.lib" )   //http
//#pragma comment(lib,   "Netapi32.lib" )//getmac

#define PEM_STRING_LICENSE "LICENSE DATA"

///许可验证结果
enum LICENSEVERFIY
{
	VERFIY_OK  = 0,//验证
	VERFIY_NoFile,//无许可证
	VERFIY_InvalidLic,//证书无效
	VERFIY_InvalidIP,//IP无效
	VERFIY_InvalidMAC,//MAC地址无效
	VERFIY_InvalidDate,//已经过期
	VERFIY_UNKNOW //未知错误
};

extern int RSA_Verify(int len,unsigned char *from ,unsigned char *to,RSA *rsa);

/**
* @brief 检查IP是否在指定列表中
* @param iplist IP地址列表,用分号进行分隔 形式如下:192.168.16.22;192.168.16.23;192.168.16.223
*/
extern int CheckIP(const char* iplist,const char *ip);
extern int LicenseVerify(const char *filename,const char *ip);

/// 获取MAC地址
extern std::string GetMac(const char *ip);

/**
*	@brief 报告服务器状态
*  @param startType 运行类型 1=开启 0=关闭
*/
extern int ReportState(int startType,const char *ip);

#endif //_KOP_LICENSE_H_